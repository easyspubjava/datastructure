# DataStructure

0. [자료구조에 대하여...](https://gitlab.com/easyspubjava/datastructure/-/blob/main/00/README.md)

1. [리스트](https://gitlab.com/easyspubjava/datastructure/-/blob/main/01/README.md)
- [배열](https://gitlab.com/easyspubjava/datastructure/-/blob/main/01/01-01/README.md)
- [연결리스트](https://gitlab.com/easyspubjava/datastructure/-/blob/main/01/01-02/README.md)

2. 스택
- [스택 이론 및 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/02/01/README.md)
- [미로찾기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/02/02/README.md)

3. 큐
- [큐 이론 및 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/03/01/README.md)
- [큐 응용프로그램](https://gitlab.com/easyspubjava/datastructure/-/blob/main/03/02/README.md)

4. [재귀함수](https://gitlab.com/easyspubjava/datastructure/-/blob/main/04/README.md)

5. 트리
- [이진트리 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/05/01/README.md)
- [힙 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/05/02/README.md)
- [이진 검색 트리 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/05/03/README.md)

6. 그래프
- [그래프 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/06/01/README.md)
- [Minimum Spanning Tree](https://gitlab.com/easyspubjava/datastructure/-/blob/main/06/02/README.md)
- [최단거리 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/06/03/README.md)

7 정렬


8 해싱

