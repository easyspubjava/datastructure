# 배열 (array)

## 1. 배열이란 ?

- 연속적인 자료구조

- 정해진 크기(fixed-length)로 시작함

- 논리적 순서와 물리적 순서가 동일(연속된 메모리 사용)

- 0번 인덱스부터 시작

- 자료가 연속적으로 저장되어야 하므로 중간에 자료가 추가되거나 삭제되는 경우 다른 자료의 이동이 필요함


## 2. 배열에 자료 추가하기
![arrayinsert](./img/arrayinsert.png)


## 3. 배열에서 자료 삭제하기
![arraydelete](./img/arraydelete.png)


## 4. 배열 사용시 장, 단점

- 배열은 **물리적, 논리적으로 연속된 자료구조**이므로 **중간에 자료를 추가하거나 삭제 할때 사용되는 시간 복잡도가 배열 요소(element) 개수에 비례한다. O(n)**

- 배열은 물리적, 논리적으로 연속된 자료구조이므로 **i번째 요소를 찾는데 드는 시간이 상수 O(1)이다**. 즉, 항상 산술적으로 계산가능하며 
이를 **인덱스 연산 []** 이라고 한다.

- 연결 리스트(linked list)보다 구현이 쉽다. 


### [Java로 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/01/01-01/java/README.md)



### [C로 구현하기](https://gitlab.com/easyspubjava/datastructure/-/blob/main/01/01-01/c/README.md)


